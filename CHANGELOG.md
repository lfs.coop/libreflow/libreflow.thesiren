# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html)[^1].

<!---
Types of changes

- Added for new features.
- Changed for changes in existing functionality.
- Deprecated for soon-to-be removed features.
- Removed for now removed features.
- Fixed for any bug fixes.
- Security in case of vulnerabilities.

-->

## [Unreleased]

## [0.4.14] - 2022-01-27

### Changed

* Limit libreflow required major version to keep libreflow and libreflow.thesiren packages compatible.

## [0.4.13] - 2021-12-16

### Added

* Use Libreflow preset system for *Publish OK* options

## [0.4.12] - 2021-12-06

### Added

* Build Render Scene: pass shot Kitsu duration to render setup operator

## [0.4.11] - 2021-11-25

### Added

* Set Blender version used in the shot compositing department to 2.93.
* Build Render Scene: add render setup operator
* Build Render Scene: add animation movie as camera BG image and save render scene
* Playblast: add options to reduce texture sizes and set target texture size

## [0.4.10] - 2021-10-12

### Changed

* Build Render Scene: do not create a working copy for *passes* directory

### Added

* An asset browser at the top of the library which can be refreshed

## [0.4.9] - 2021-10-01

### Added

* Build Blocking Scene: add option to automate more steps which may not be stable enough to always use

### Added

* A new file `design.ai` in the list of files to configure for a prop.

## [0.4.8] - 2021-09-01

### Added

* A new `compositing` departement in assets
* Shot department dependencies (queried to build a scene for instance) can be defined in a dependency template with the same name of the department.
* Two files `modelisation.blend` and `modelisation_export.obj` in an asset's list of configurable modeling files.

### Changed

* The render scene building dependency list displays the last revision of all file component of each dependency.

### Fixed

* Allow to build a render scene from an `Animation.blend` scene.
* Do not overwrite folder if already existing

## [0.4.7] - 2021-07-20

### Added

* *audio_voices.wav* and *audio_effects.wav* files in the *misc* department are part of layout and blocking scene dependencies (in addition to *audio.wav* files) for the scene building.
* A folder in the list of files and folders to be configured for a set, aimed at storing additional layers.

### Changed

* Blocking scene building action has been updated, in accordance with changes made to the Blender scene building add-on to update audio files in such scenes.

## [0.4.6] - 2021-07-07

### Fixed

* Set `BLENDER_EXEC_PATH` variable in environment before starting to build a Blender scene.
* Use Libreflow `status` icons with lower case names (instead of Kabaret native ones) to fix icon lookup error on Linux.

## [0.4.5] - 2021-06-18

### Fixed

* Disable cleanup operator when building a layout scene, which made sets unselectable.

## [0.4.4] - 2021-06-09

### Added

* A new action to build a compositing scene.
* JobsView and JobsActor have been registered in session for job management.

### Fixed

* Working copies are deleted with `new publication` option selected when configuring the files of an asset.

## [0.4.3] - 2021-06-01

### Added

* A new asset utility department `Miscellaneous` added alongside `Rig Modules`.

### Changed

* Rig modules are represented in the flow by a single `Util` department.

## [0.4.2] - 2021-05-27

### Added

* Added runner descriptions in scene building actions.
* Added trailing `_` to asset collection names at blocking scene building.

## [0.4.1] - 2021-05-11

### Changed

* File path and prefix are defined and cached at the map level instead of the department's using Libreflow's `get_context_value` utility function (Libreflow required version: [1.6.1](https://pypi.org/project/libreflow/1.6.1/)).
* `file_prefix` and `file_path` params have been defined in objects of both asset and shot branches, to ease the management of file prefixes and paths wherever files are needed.

### Added

* A new misc department has been added at the sequence level.

## [0.4.0] - 2021-05-11

### Added

* Libreflow's SubprocessView inherited from `kabaret.subprocess_manager` is now available in the GUI's list of view. Beware access to this view depends on the Libreflow [1.6.0](https://pypi.org/project/libreflow/1.6.0/) release, which introduced the feature.

## [0.3.9] - 2021-05-11

### Changed

* Shot *comp* department has been renamed *compositing*.

## [0.3.8] - 2021-05-11

### Added

* A new *comp* deparment has been added in the shot departments to contain compositing files.
* Search filters have been enabled on maps at several level of the flow, in both shot and asset sides.

### Fixed

* Blocking scene can be built only if the layout scene's last revision is available on the current site.

## [0.3.7] - 2021-04-30

### Added

* An action in the Animation department now allows to create a `animation.blend` file. If a non empty blocking scene (either named `breakdown.blend` or `blocking.blend`) exists in the list, the user can select in the prompted dialog a revision to create a working copy from. Otherwise, the action will create an empty file without prompting a dialog.

### Changed

* Blocking scenes have been more explicitly renamed *blocking.blend*.

## [0.3.6] - 2021-04-20

### Added

* Tracked file dependencies can now be displayed and requested by means of a `GetDependencies` action.
  - Theoretical dependencies are collected from predefined dependency templates, wichi include the Kitsu casting of the element the file belongs to (only shot as of now).
  - Real dependencies are retrieved from the Blender Asset Tracer report of a given revision.

## [0.3.5] - 2021-04-16

### Fixed

* Save built Blender scene after setup to prevent conflicts between template and target files drive location.

## [0.3.4] - 2021-04-16

### Added

* Login page shows up when accessing sequences and assets if user is not connected to Kitsu.

## [0.3.3] - 2021-04-12

### Changed

* Remap absolute paths to relative when creating layout Blender files
* Add assets to the proper collection when creating layout Blender files
* Add action to create animation (breakdown) scenes from layout

## [0.3.2] - 2021-04-02

### Changed

* Intermediate *templates* folder has been removed from asset utils folder hierarchy.
* Default Blender template now disables viewport denoise for a more responsive interface

## [0.3.1] - 2021-04-02

### Added

* Asset types now embed a section dedicated to asset utilities (skinning weights, rig modules, etc.).

## [0.3.0] - 2021-04-01

### Changed

* UI feedbacks on asset creation action simply rely on warn messages instead of field highlighting.

### Fixed 

* the libreflow b3d utils allow to wrap the python expression of the layout building tool so it works when calling blender through a .bat file.
* The user update at startup might crash libreflow if the user hasn't connected yet. Fixed with a shinny try/expect.
* Default values have been set in the settings to avoid issues while creating a new project.

## [0.2.1] - 2021-03-26

### Fixed

* A workaround has been added to make GUI display on latest MacOS version ([Kabaret issue #96](https://gitlab.com/kabaretstudio/kabaret/-/issues/96)).

## [0.2.0] - 2021-03-22

### Added

* User class is now handled by injection:
  * User last login, libreflow and libreflow.thesiren version is storred in their profile

### Fixed

* Tracked folders now have the "Publish OK" option available.

## [0.1.10] - 2021-03-18

## Added

* Publish OK: When publishing changes made in a file, users can choose to publish these in another file (suffixed with `_ok`). This way, this procedure allows to keep track of determinant (and shareable) revisions exclusively.

## [0.1.9] - 2021-03-09

### Fixed

* The end frame of the layout scene has been corrected at building.

## [0.1.8] - 2021-03-09

### Added

* Displayed version of libreflow and libreflow.thesiren in the /Home page. That allows to know when to update Libreflow.


## [0.1.7] - 2021-03-05

### Fixed

* Added FBX file format in PyPI package data to make FBX template available.

## [0.1.6] - 2021-03-05

### Added

* A new action allows to create an asset registered in the Kitsu database, and configure its files.

## [0.1.5] - 2021-03-04

### Fixed

* Add missed Blender operator call in scene building action for layout scene setup.

## [0.1.4] - 2021-03-04

### Changed

* Users can build a layout scene with missing dependencies. In this case, and for now, a warning message reminds that the scene will have to be manually updated later.

## [0.1.3] - 2021-03-01

### Added

* A new action in layout department for building Blender scenes, based on Kitsu information.

### Changed

* A new Blender template file with no collection, no object, and the right start frame (101), FPS (24), resolution (2048x854), color space and transparent background (/film) at render.

### Fixed

* Project object is systematically touched when accessed from a home page widget to ensure user environment update.

## [0.1.1] - 2021-02-15

### Changed

Ai file format have been move in main libreflow package.

### Fixed

Deprecated references to flag enabling file system operations have been removed.

## [0.1.0] - 2021-02-08

### Added

- Added a films map at project's root to allow project flow and test flow on a test movie
- Added the right asset library description with a asset family 
- Added many basic departements for the library and the shots
- Added a Custom Home for this project, so far it's the same as libreflow.examples.majorque's but will be tuned soon.
- Added a custom GUI, allowing to call the new custom Home
- Added a custom Style, allowing us to fix issues with released style, and make tweaks for this project.
- AI and WAV file formats made available for the production
- Custom TrackedFolder injection to handle the new lib filenames righ

### Changed

- The home page has been changed
- Minimum libreflow requiered version is now 1.2.0

## [0.0.4] - 2021-02-05

### Fixed

Rename source folder in accordance to PyPI package name

## [0.0.3] - 2021-02-05

Configuration for auto deployment

## [0.0.1] - 2021-02-04

Initial public commit and PyPI setup. This version is an early version of libreflow.the_siren. It defines a flow derived from Libre Flow's baseflow, and designed to support the workflow and dataflow involved in an animation feature film project.

### Fixed

libreflow.the_siren is now ready for use as a pip package
